// The sum of the squares of the first ten natural numbers is,
// $$1^2 + 2^2 + ... + 10^2 = 385$$
// The square of the sum of the first ten natural numbers is,
// $$(1 + 2 + ... + 10)^2 = 55^2 = 3025$$
// Hence the difference between the sum of the squares of the first ten natural
// numbers and the square of the sum is $3025 - 385 = 2640$.
// Find the difference between the sum of the squares of the first one hundred
// natural numbers and the square of the sum.

// The solution to solve this is pretty much given in the description, it just
// needs to be expressed in code.

let sumOfSquare x = 
    x
    |> List.map (fun item -> item * item)
    |> List.sum

let squareOfSum x =
    x
    |> List.sum
    |> pown <| 2

let sumOfSquare100 = 
    [1..100]
    |> sumOfSquare

let squareOfSum100 = 
    [1..100]
    |> squareOfSum

squareOfSum100 - sumOfSquare100 