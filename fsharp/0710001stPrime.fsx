// By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
// that the 6th prime is 13.
// What is the 10 001st prime number?

// Here we can borrow a bit of logic from problem 03 about prime factors, as we
// generate primes in that one to get the possible factors.
// Finding the next prime can be done by checking if a given number is divisible
// with any of the already known primes, up to the largest prime that is <= the
// square root of the number being checked.
// A simple optimization is to discard all even numbers except 2 as these are
// not primes.

let generatePrimes x =
    match x with
    | 1 -> [2]
    | i -> 
        Seq.unfold (fun state -> 
        // State stores primes until the length of i is reached
        // unfold inside this unfold runs until next prime is found, starting 
        // largest prime in state for the attempted one, and incrementing by 2
        // every time (ignoring even numbers)




