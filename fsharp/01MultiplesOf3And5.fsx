// If we list all the natural numbers below 10 that are multiples of 3 or 5, we
// get 3, 5, 6 and 9. The sum of these multiples is 23.
// Find the sum of all the multiples of 3 or 5 below 1000.

// Set the max, not inclusive
let limit = 1000

// Define method for if something is a multiple
let IsMultiple denominator value = 
    value % denominator = 0

// Partially apply with the multiples we want to check for
let IsMultipleOf5 = IsMultiple 5
let IsMultipleOf3 = IsMultiple 3

// Combine the above to one
let IsMultipleOf3Or5 value = IsMultipleOf3 value || IsMultipleOf5 value

// Generate a list of 0 to limit (not inclusive) and sum up only multiples
let SumOfMultiplesOf3Or5 = 
    [0..limit-1]
    |> List.fold (fun sum x -> if IsMultipleOf3Or5 x then sum + x else sum) 0