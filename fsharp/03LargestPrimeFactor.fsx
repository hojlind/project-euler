// The prime factors of 13195 are 5, 7, 13 and 29.
// What is the largest prime factor of the number 600851475143

let divisibleBy (denominator:int64) value =
    value % denominator = int64 0

let rec getPrimeFactors (value:int64) : int64 list = 
    match value with
    | i when i < 2 -> []
    | _ -> 
        if (divisibleBy 2 value) then
            2 :: getPrimeFactors (value / (int64 2))
        else
            let denominator =
                Seq.unfold (fun x -> 
                    if (float x) <= sqrt (float value) then 
                        Some(x, x + 2) 
                    else
                        None)
                    3
                |> Seq.tryFind(fun x -> divisibleBy x value)

            match denominator with
            | Some number -> number :: getPrimeFactors (value / (int64 number))
            | None -> [ value ]


let result = getPrimeFactors 600851475143L