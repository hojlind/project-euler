// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
// Find the largest palindrome made from the product of two 3-digit numbers.

// New approach, simply splitting the number into individual digits, then
// comparing the reverse, which must be the same
let digits n = 
    n
    |> Seq.unfold (fun state -> 
       match state with 
       | 0 -> None
       | i -> Some(i % 10, i / 10))
    |> Seq.toList

let isPalindrome x = 
    let xdigits = digits x
    xdigits = (List.rev xdigits)

isPalindrome 900099
isPalindrome 990099

let maxPalindrome = 
    let mutable maxPal = 0
    for i in 100..999 do
        for j in 100..i do
            if isPalindrome (i*j) then
                maxPal <- max maxPal (i*j)
    maxPal
// Failed approach, thought was to match first and last, and recursively call
// with those stripped away, all done through mathematical functions.
// However this does not work with numbers that contains 0's as these are
// stripped, resulting in the loss of digits.
(*
let digitCount x = 
    match x with
    | 0 -> 0
    | _ -> 
        float x
        |> log10
        |> (+) (float 1)
        |> floor
        |> int


let rec isPalindrome x =
    let digits = digitCount x
    match digits with
    | 0 | 1 -> true
    | i when i > 1 -> 
        let highDigit = 
            float x |> (/) <| 10.0 ** float((i - 1))
            |> int
        let lowDigit = x % 10
        let newValue = 
            float x |> (-) <| (float highDigit |> ( * ) <| 10.0 ** float((i - 1)))
            |> int
            |> (/) <| 10
        printfn "%i" newValue
        highDigit = lowDigit && isPalindrome newValue

    | _ -> false

let largestPalindrome = 
    (999,999)
    |> Seq.unfold (fun (a,b) -> if a > 100 then Some ((a*b), (a-1,b)) else None)
    |> Seq.find (fun x -> isPalindrome x)

isPalindrome 900099
*)