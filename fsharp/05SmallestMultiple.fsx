// 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
// What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

// There is only ever one smallest multiple of a set of numbers.
// The next multiple of a set of numbers is always an even multiple of the
// smallest multiple. e.g. smallest of 2 and 3 is 6, next is 12 and then 18.
// If we wish to add a new higher number to the set, the smallest possible
// multiple must be a multiple of the smallest multiple of the original set.
// The initial way is slightly bruteforceish by simply running through the
// multiples of the larger number until the lower number also matches. 

let smallestMultiple x y =
    let largest = max x y
    let smallest = min x y

    // We know that the smallest multiple of the two must be a multiple of the
    // largest.
    let multiplesOfLargest = 
        largest 
        |> Seq.unfold (fun state -> Some(state, state + largest))

    // Find the first item that is a multiple of the smallest, this is the
    // smallest shared multiple.
    multiplesOfLargest 
    |> Seq.find (fun item -> item % smallest = 0 )

// Helper for getting range from 1..x
let smallestMultipleOfRange x =
    [1..x]
    |> Seq.reduce (fun acc item -> smallestMultiple acc item)


// An alternative, and perhaps smarter method, is to use prime factorization to
// figure out the lowest common multiple for two numbers. This is done by
// breaking each of the numbers down to their prime factors and merging these
// with the single rule:
// For each prime select the highest occurence count and add to a new list of
// factors.
// e.g. the prime factors of 6 is 2 * 3, and of 8 is 2 * 2 * 2. We go over each 
// prime starting with two. In six, 2 occurs once, in 8 it occurs 3 times. 
// therefore we add 3 2's to the prime factors making up the common multiple.
// 3 only exists once and in one place, so that gets added, making the common
// multiples prime factors 2 * 2 * 2 * 3 = 24
// Performance wise this intuitively seems slower, especially for smaller ranges
// however with memoisation of prime factors I think it might end up faster.
// Below is an attempt to implement this solution.

// We borrow the prime factorization code from problem 3, but change it to deal
// with regular ints for ease of use. For bigger numbers than this assignment
// needs you might wish to change that.


// Get the prime factors that make up a given number
let rec getPrimeFactors value = 

    let divisibleBy denominator value =
        value % denominator = 0

    match value with
    | i when i < 2 -> []
    | _ -> 
        if (divisibleBy 2 value) then
            2 :: getPrimeFactors (value / 2)
        else
            let denominator =
                Seq.unfold (fun x -> 
                    if (float x) <= sqrt (float value) then 
                        Some(x, x + 2) 
                    else
                        None)
                    3
                |> Seq.tryFind(fun x -> divisibleBy x value)

            match denominator with
            | Some number -> number :: getPrimeFactors (value / number)
            | None -> [ value ]


let groupPrimeFactors factors = 
    factors
    |> List.countBy (fun item -> item)

let flattenPrimeFactorGroup group =
    group
    |> List.fold (fun state (key, count) -> state @ (List.replicate count key)) []

let getAndGroupPrimeFactors x =
    getPrimeFactors x
    |> groupPrimeFactors
 

let smallestMultipleOfRangePrime x = 
    [1..x]
    |> List.map getAndGroupPrimeFactors
    |> List.collect (fun item -> item)
    |> List.fold (fun state item ->
        match item with
        | (prime, count) when List.exists (fun (a, b) -> a = prime && b < count) state ->
            // Replace the existing group with the new one, with a larger count.
            item :: (List.filter (fun (a, b) -> a <> prime) state)
        | (prime, count) when List.exists (fun (a, b) -> a = prime && b >= count) state ->
            // The existing group is bigger or equal, item can be discarded.
            state
        | item ->
           state @ [ item ] 
    ) []
    |> flattenPrimeFactorGroup
    |> List.reduce ( * )
